create or replace PACKAGE JointVentureInterface AS 
-- JointVentureInterface DESStoredProcedures 20200623
    FUNCTION CHKCERT2 (
		PatientId IN VARCHAR2,
		Subscript IN VARCHAR2,
		BeginDateTime IN DATE,
        EndDateTime IN DATE
	)
    RETURN NUMBER;
    
    FUNCTION mumps_dt_conv (
        gregorian_dt IN DATE
    ) RETURN NUMBER;

    PROCEDURE GET_SITE;


    PROCEDURE GETPROVIDERDATA (
        UserId              IN      VARCHAR2
    );

    PROCEDURE GET_RAD_RESULT ( 
        ExamId              IN      VARCHAR2 DEFAULT '0'
     );

    PROCEDURE GET_RADIOLOGY_EXAM_LIST_RAD_NEW (
		        patientid     IN  VARCHAR2 DEFAULT '0',
        earliestdate  IN  DATE DEFAULT NULL,
        latestdate    IN  DATE DEFAULT NULL,
        userid        IN  VARCHAR2 DEFAULT '0',
        statusall     IN  VARCHAR2
	);

    PROCEDURE GET_RAD_AMENDMENTS ( 
        ExamId            IN      VARCHAR2    DEFAULT '0'

    );

    PROCEDURE GetRadClinicalImpression (
        orderId VARCHAR2 DEFAULT '0'
    );

    PROCEDURE GET_PDTS_ALLERGY_PROFILE
    (
     patientId VARCHAR2 DEFAULT '0'
    );

    PROCEDURE GET_PATIENT_IMMUNIZATIONS
    (
     patientId VARCHAR2 DEFAULT '0'
    );

    PROCEDURE GET_PATIENT_ENCOUNTERS
    (
    	PatientId	IN 		varchar2,
		StartDate	IN 		Date,
		StopDate	IN 		Date,
		"Max"		IN 		Number,
		pALL 		IN 		Number
    );

    PROCEDURE GET_PAT_PRESCRIPTIONS
    ( 
        patientid     IN  VARCHAR2,
        earliestdate  IN  DATE DEFAULT sysdate - 548,
        latestdate    IN  DATE DEFAULT sysdate + 1,
        activeonly    IN  NUMBER DEFAULT 1
     );

    PROCEDURE GETPRESCRIPTIONFILLS
    ( 
    RXID IN VARCHAR2
     );

     PROCEDURE GET_PATIENT_NOTES_CDA 
    (
        patientid       VARCHAR2 DEFAULT '0',
        earliestdate    DATE,
        latestdate      DATE,
        notetype        VARCHAR2 DEFAULT 'DCPO'
    );

    PROCEDURE GET_PATIENT_PROBLEM_LIST_BYDATE
    ( 
    	PatientId 		IN 		VARCHAR2,
    	EarliestDate 	IN 		DATE,
    	LatestDate 		IN 		DATE
    );

    PROCEDURE GET_PATIENT_PROCEDURES
 	(
 		PatientId 		IN VARCHAR2,
		EarliestDate	IN DATE,
		LatestDate		IN DATE
 	);

    PROCEDURE GET_SNOMEDCODES
    ( 
    PatientId IN VARCHAR2,
    ResultId IN VARCHAR2
     );

    PROCEDURE GET_SPECIMENS
    ( 
    ResultId IN VARCHAR2
     );

    PROCEDURE GET_LABORDER_LIST
   (
        patientid           VARCHAR2 DEFAULT '0',
        earliestdate        DATE DEFAULT NULL,
        latestdate          DATE DEFAULT NULL,
        targetsubcriptlist  VARCHAR2 DEFAULT 'NA'
    );

     PROCEDURE GET_LABMICROBIOLOGY_RESULT
    ( 
        TaskId IN VARCHAR2,
        ResultId IN VARCHAR2,
        User 	IN VARCHAR2
     );

      PROCEDURE GET_LABMICRO_ORGSENSITIVITY
    ( 
        patientid  IN  VARCHAR2,
        resultid   IN  VARCHAR2,
        testid     IN  VARCHAR2,
        orgid      IN  VARCHAR2
     );

     PROCEDURE GET_LABMICRO_ORGANISMS
    ( 
        PatientId IN VARCHAR2,
        ResultId IN VARCHAR2,
        TestId IN VARCHAR2
    );

    PROCEDURE GET_LAB_CHEMISTRY_RESULT
    ( 
        PatientId IN VARCHAR2,
        ResultId IN VARCHAR2,
        TestId IN VARCHAR2
    );

    PROCEDURE GET_LAB_PATHOLOGY_RESULT
    ( 
        PatientId IN VARCHAR2,
        ResultId IN VARCHAR2
    );

END JointVentureInterface;
/

create or replace PACKAGE BODY                                                                       jointventureinterface AS 
-- JointVentureInterface DESStoredProcedures 20200623

    FUNCTION chkcert2 (
        patientid      IN  VARCHAR2,
        subscript      IN  VARCHAR2,
        begindatetime  IN  DATE,
        enddatetime    IN  DATE 
    ) RETURN NUMBER AS
        l_cert_date   NUMBER(3);
        v_start_date  TIMESTAMP;
        v_end_date    TIMESTAMP;
    BEGIN
        v_start_date := begindatetime;       --to_timestamp(begindatetime, 'YYYYMMDD HH24:MI:SS');
        v_end_date := enddatetime;      --to_timestamp(enddatetime, 'YYYYMMDD HH24:MI:SS');
		--check blood bank, micro and chemistry
        CASE
            WHEN subscript IN (
                'BB',
                'BT',
                'CH',
                'MY',
                'OP',
                'TB',
                'VR'
            ) THEN
                CASE
                    WHEN subscript = 'BB' THEN
                        l_cert_date := 0;
                        SELECT
                            COUNT(cd.certify_datetime)
                        INTO l_cert_date
                        FROM
                           sub_blood_bank_63_59    sb,
                           lab_result_63           lr,
                           sub_result_63_794       cd
                        WHERE
                                sb.lab_result_63 = lr.ien
                            AND sb.ien = cd.sub_blood_bank_63_59
                            AND lr.patient = patientid
                            AND cd.certify_datetime BETWEEN v_start_date AND v_end_date;

                    WHEN subscript = 'BT' THEN
                        SELECT
                            COUNT(cd.certify_datetime)
                        INTO l_cert_date
                        FROM
                           sub_bacteriology_63_6    sb,
                           lab_result_63            lr,
                           sub_test_63_737          cd
                        WHERE
                                sb.lab_result_63 = lr.ien
                            AND sb.row_id = cd.sub_bacteriology_63_6
                            AND lr.patient = patientid
                            AND cd.certify_datetime BETWEEN v_start_date AND v_end_date;

                    WHEN subscript = 'CH' THEN
                        SELECT
                            COUNT(cd.certify_datetime)
                        INTO l_cert_date
                        FROM
                           sub_clinical_chemistry_63_04      sb,
                           lab_result_63                     lr,
                           sub_result_63_07                  cd
                        WHERE
                                sb.lab_result_63 = lr.ien
                            AND sb.row_id = cd.sub_clinical_chemistry_63_04
                            AND lr.patient = patientid
                            AND cd.certify_datetime BETWEEN v_start_date AND v_end_date;

                    WHEN subscript = 'MY' THEN
                        l_cert_date := 0;
                        SELECT
                            COUNT(cd.certify_datetime)
                        INTO l_cert_date
                        FROM
                           sub_mycology_63_735    sb,
                           lab_result_63          lr,
                           sub_test_63_753        cd
                        WHERE
                                sb.lab_result_63 = lr.ien
                            AND sb.ien = cd.sub_mycology_63_735
                            AND lr.patient = patientid
                            AND cd.certify_datetime BETWEEN v_start_date AND v_end_date;

                    WHEN subscript = 'OP' THEN
                        SELECT
                            COUNT(cd.certify_datetime)
                        INTO l_cert_date
                        FROM
                           sub_parasitology_63_33    sb,
                           lab_result_63             lr,
                           sub_test_63_746           cd
                        WHERE
                                sb.lab_result_63 = lr.ien
                            AND sb.ien = cd.sub_parasitology_63_33
                            AND lr.patient = patientid
                            AND cd.certify_datetime BETWEEN v_start_date AND v_end_date;

                    WHEN subscript = 'TB' THEN
                        l_cert_date := 0;
                        SELECT
                            COUNT(cd.certify_datetime)
                        INTO l_cert_date
                        FROM
                           sub_mycobacterium_63_734    sb,
                           lab_result_63               lr,
                           sub_test_63_761             cd
                        WHERE
                                sb.lab_result_63 = lr.ien
                            AND sb.ien = cd.sub_mycobacterium_63_734
                            AND lr.patient = patientid
                            AND cd.certify_datetime BETWEEN v_start_date AND v_end_date;

                    WHEN subscript = 'VR' THEN
                        l_cert_date := 0;
                        SELECT
                            COUNT(cd.certify_datetime)
                        INTO l_cert_date
                        FROM
                           sub_virology_63_736    sb,
                           lab_result_63          lr,
                           sub_test_63_768        cd
                        WHERE
                                sb.lab_result_63 = lr.ien
                            AND sb.ien = cd.sub_virology_63_736
                            AND lr.patient = patientid
                            AND cd.certify_datetime BETWEEN v_start_date AND v_end_date;

                    ELSE
                        l_cert_date := 0;
                END CASE;

		--get ap cert date/time				 
            WHEN subscript IN (
                'AU',
                'BM',
                'SP'
            ) THEN
                CASE

                    WHEN subscript = 'AU' THEN
                        SELECT
                            COUNT(sb.certify_datetime)
                        INTO l_cert_date
                        FROM
                            sub_autopsy_63_46    sb,
                            lab_result_63        lr
                        WHERE
                                sb.lab_result_63 = lr.ien
                            AND lr.patient = patientid
                            AND sb.certify_datetime BETWEEN v_start_date AND v_end_date;

                    WHEN subscript = 'BM' THEN
                        SELECT
                            COUNT(sb.certify_datetime)
                        INTO l_cert_date
                        FROM
                           sub_bone_marrow_63_47    sb,
                           lab_result_63            lr
                        WHERE
                                sb.lab_result_63 = lr.ien
                            AND lr.patient = patientid
                            AND sb.certify_datetime BETWEEN v_start_date AND v_end_date;

                    WHEN subscript = 'SP' THEN
                        SELECT
                            COUNT(sb.certify_datetime)
                        INTO l_cert_date
                        FROM
                           sub_surgical_pathology_63_03    sb,
                           lab_result_63                   lr
                        WHERE
                                sb.lab_result_63 = lr.ien
                            AND lr.patient = patientid
                            AND sb.certify_datetime BETWEEN v_start_date AND v_end_date;
                    ELSE
                        l_cert_date := 0;

                END CASE;
            WHEN subscript IN (
                'CG',
                'CN'
            ) THEN
                CASE

                    WHEN subscript = 'CG' THEN
                        SELECT
                            COUNT(sb.certify_datetime)
                        INTO l_cert_date
                        FROM
                           sub_cytology_gyn_63_44    sb,
                           lab_result_63             lr
                        WHERE
                                sb.lab_result_63 = lr.ien
                            AND lr.patient = patientid
                            AND sb.certify_datetime BETWEEN v_start_date AND v_end_date;

                    WHEN subscript = 'CN' THEN
                        SELECT
                            COUNT(sb.certify_datetime)
                        INTO l_cert_date
                        FROM
                           sub_cytology_nongyn_63_727    sb,
                           lab_result_63                 lr
                        WHERE
                                sb.lab_result_63 = lr.ien
                            AND lr.patient = patientid
                            AND sb.certify_datetime BETWEEN v_start_date AND v_end_date;

                    ELSE
                        l_cert_date := 0;

                END CASE;
            ELSE
                l_cert_date := 0;

        END CASE;

        RETURN l_cert_date;
    END chkcert2;

    FUNCTION mumps_dt_conv (
        gregorian_dt IN DATE
    ) RETURN NUMBER AS

        v_greg_dt      DATE;
        date_inverter  CONSTANT NUMBER := 9999999;
        h_date         NUMBER;
        century_ind    VARCHAR2(1);
        mumps_date     NUMBER;
        input_century  NUMBER; 
        cent_base_val   constant NUMBER := 18;
        cent_base_code  constant NUMBER := 0;


        TYPE century_cd_typ IS RECORD (
            century_value  NUMBER,
            century_code   NUMBER
        );
        TYPE century_cd_tab IS
            TABLE OF century_cd_typ INDEX BY BINARY_INTEGER;
        l_century_cd   century_cd_tab;

    BEGIN

        v_greg_dt := to_date(to_char(gregorian_dt, 'yyyy-mm-dd'), 'yyyy-mm-dd');

        For i in 1..10 
        Loop
            l_century_cd(i).century_value := cent_base_val + i;
            l_century_cd(i).century_code := cent_base_code + i;
        end loop;

        input_century := to_number(to_char(v_greg_dt, 'cc'));
        FOR i IN 1..10 LOOP
            IF l_century_cd(i).century_value = input_century THEN
                century_ind := l_century_cd(i).century_code;
            END IF;
        END LOOP;

        h_date := to_number(century_ind || to_char(v_greg_dt, 'yymmdd'));
        mumps_date := date_inverter - h_date;
        RETURN mumps_date;
    EXCEPTION
        WHEN OTHERS THEN
            mumps_date := 99999999;
            RETURN mumps_date;
    END mumps_dt_conv;


    PROCEDURE get_site AS
        refcursor SYS_REFCURSOR;
    BEGIN
        OPEN refcursor FOR SELECT
                               a1."NAME"            AS site_name,
                               a1.mtf_code          AS site_code,
                               b1.facility_name     AS dmis_site_name,
                               b1.dmis_id           AS dmis_id_code,
                               a1.ien               AS site_ien,
                               a1.street_addr__1    AS site_address,
                               a1.city              AS city,
                               c1.abbreviation      AS "STATE",
                               d1.code              AS zipcode,
                               c2."NAME"            AS country,
                               a1.phone_            AS phone
                           FROM
                                    medical_treatment_facility_4 a1
                               JOIN dmis_id_codes_8103     b1 ON a1.site_id = b1.site_id
                                                             AND a1.dmis_id = b1.ien
                               JOIN geographic_location_5  c1 ON a1.site_id = c1.site_id
                                                                AND a1.stategeog_loc = c1.ien
                               JOIN geographic_location_5  c2 ON a1.site_id = c2.site_id
                                                                AND nvl(c1.country_name, c1."NAME") = c2."NAME"
                               LEFT OUTER JOIN zip_code_5_8002        d1 ON a1.site_id = d1.site_id
                                                                     AND a1.zip = d1.ien
                           WHERE
                                   a1.ien = '5142' -- must be barcoded for Seymour Johnson
                               AND a1.site_id = 63;

        dbms_sql.return_result(refcursor);
    END get_site;

    PROCEDURE getproviderdata (
        userid IN VARCHAR2
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN
        OPEN refcursor FOR SELECT
                               userid,
                               a1.name                       AS name,
                               a1.office_street_address_1    AS street,
                               a1.office_city                AS city,
                               a1.office_state_name          AS state,
                               a1.office_zipcode_code        AS zip,
                               b1.country_name               AS country,
                               a1.duty_phone_1               AS tel
                           FROM
                              provider_6               a1,
                              geographic_location_5    b1
                           WHERE
                                   a1.office_state = b1.ien
                               AND a1.site_id = b1.site_id
                               AND a1.site_id = 63
                               AND a1.ien = userid;

        dbms_sql.return_result(refcursor);
    END getproviderdata;

    PROCEDURE get_rad_result (
        examid VARCHAR2 DEFAULT '0'
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN
        OPEN refcursor FOR SELECT
                               b1.name                                           AS approving_radiologist,
                               b2.name                                           AS interpreting_radiologist,
                               a1.datetime_created                               AS datetime_created,
                               a1.report_status                                  AS report_status,
                               a1.ien                                            AS id,
                               f1.diagnostic_code                                AS result_code,
                               f1.description                                    AS result_code_description,
                               e1.name                                           AS verified_by,
                               a1.verified_date                                  AS verified_date,
                               b3.name                                           AS supervising_radiologist,
                               c1.dmis_id_code                                   AS dmis_id,
                               a1.recorders_division_name                        AS dmis_id_name,
                               a1.date_verified_by_supervisor                    AS date_supervised,
                               decode(i1.status, NULL, 'FALSE', 'TRUE')          AS amended,
                               d1.report_text                                    AS report_text,
                               e2.name                                           AS transcribed_by
                           FROM
                               radiology_reports_74 a1
                               JOIN provider_6 b1 ON a1.site_id = b1.site_id
                               AND a1.approving_radiologist = b1.ien
                               JOIN provider_6 b2 ON a1.site_id = b2.site_id
                               AND a1.interpreting_radiologist = b2.ien
                               JOIN medical_center_division_40_8 c1 ON a1.site_id = c1.site_id
                               AND a1.recorders_division = c1.ien
                               JOIN sub_report_text_74_02 d1 ON a1.site_id = d1.site_id
                               AND a1.ien = d1.radiology_reports_74
                               JOIN user_3 e1 ON a1.site_id = e1.site_id
                               AND a1.verified_by = e1.ien
                               JOIN user_3 e2 ON a1.site_id = e2.site_id
                               AND a1.recorded_by = e2.ien
                               JOIN result_category_78_3 f1 ON a1.site_id = f1.site_id
                               AND a1.result_code = f1.ien
                               JOIN radiology_exam_70_5 g1 ON a1.site_id = g1.site_id
                               AND a1.patient_name = g1.name
                               JOIN patient_2 h1 ON a1.site_id = h1.site_id
                               AND a1.patient_name = h1.ien                               
                               inner join MEDICAL_CENTER_DIVISION_40_8 j1 on a1.RECORDERS_DIVISION = j1.ien
                               and a1.site_id = j1.site_id 
                               inner join DMIS_ID_CODES_8103 k1 on j1.DMIS_ID_CODE = k1.ien
                               and j1.site_id = k1.site_id
                               LEFT OUTER JOIN provider_6 b3 ON a1.site_id = b3.site_id
                               AND a1.supervising_radiologist = b3.ien
                               LEFT OUTER JOIN sub_amended_report_74_05 i1 ON a1.site_id = i1.site_id
                               AND a1.ien = i1.radiology_reports_74
                           WHERE
                               g1.ien = examid
                               AND a1.site_id = 63;

        dbms_sql.return_result(refcursor);
    END get_rad_result;

    PROCEDURE get_radiology_exam_list_rad_new (
        patientid     IN  VARCHAR2 DEFAULT '0',
        earliestdate  IN  DATE DEFAULT NULL,
        latestdate    IN  DATE DEFAULT NULL,
        userid        IN  VARCHAR2 DEFAULT '0',
        statusall     IN  VARCHAR2
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN
        OPEN refcursor FOR SELECT
                               a1.exam_datetime                 AS exam_datetime,
                               b1.name                          AS procedure_name,
                               b1.code                          AS procedure_code,
                               a1.exam_status_status            AS exam_status,
                               a1.ien                           AS id,
                               c1.name                          AS requesting_provider_name,
                               d1.name                          AS requesting_location_name,
                               a1.order_ien                     AS order_id,
                               a1.exam_no_                      AS exam_no,
                               d2.name                          AS radiology_location_name,
                               f1.dmis_id                       AS location_code,
                               f1.facility_name                 AS location_name,
                               a1.order_id                      AS order_no,
                               a1.order_datetime                AS order_datetime,
                               a1.order_priority_name           AS priority,
                               a1.performing_technician_name    AS technologist_name,
                               a1.procedure_03                  AS cpt_code,
                               i1.description                   AS cpt_description,
                               g1.order_comment                 AS order_comment
                           FROM
                                    radiology_exam_70_5 a1
                               JOIN radiology_procedures_71       b1 ON a1.site_id = b1.site_id
                                                                  AND a1.procedure_03 = b1.ien
                               JOIN provider_6                    c1 ON a1.site_id = c1.site_id
                                                     AND a1.requesting_hcp = c1.ien
                               JOIN hospital_location_44          d1 ON a1.site_id = d1.site_id
                                                               AND a1.req__wardclinic = d1.ien
                               JOIN hospital_location_44          d2 ON a1.site_id = d2.site_id
                                                               AND a1.radiology_location = d2.ien
                               JOIN medical_center_division_40_8  e1 ON d1.site_id = e1.site_id
                                                                       AND d1.division = e1.ien
                               JOIN dmis_id_codes_8103            f1 ON e1.site_id = f1.site_id
                                                             AND e1.dmis_id_code = f1.ien
                               JOIN order_101                     g1 ON a1.site_id = g1.site_id
                                                    AND a1.order_ien = g1.ien
                               JOIN order_priority_102_3          h1 ON g1.site_id = h1.site_id
                                                               AND g1.priority = h1.ien
                               JOIN cpthcpcs_8151                 i1 ON a1.site_id = i1.site_id
                                                        AND a1.procedure_03 = i1.ien
                               JOIN patient_2                     j1 ON g1.site_id = j1.site_id
                                                    AND g1.patient = j1.ien
                               JOIN user_3                        k1 ON k1.site_id = c1.site_id
                                                 AND k1.provider = c1.ien
                           WHERE
                                   g1.patient = patientid
                               AND a1.exam_datetime BETWEEN nvl(earliestdate, sysdate - 540) AND nvl(latestdate, sysdate)
                               AND k1.ien = userid
                               AND ( ( statusall = 'ALL' )
                                     OR ( statusall IS NULL
                                          AND a1.exam_status_status IN (
                                   'AMENDED',
                                   'COMPLETE'
                               ) ) )
                               AND a1.site_id = 63;

        dbms_sql.return_result(refcursor);
    END get_radiology_exam_list_rad_new;

    PROCEDURE get_rad_amendments (
        examid VARCHAR2 DEFAULT '0'
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN
        OPEN refcursor FOR SELECT
                b1.ien,
                 c1.amended_datetime                       AS datetime,
                 e1.name                                   AS interpreting_radiologist,
                 e2.name                                   AS approving_radiologist,
                 f1.diagnostic_code                        AS result_code,
                 f1.description                            AS result_code_description,
                 c1.verified_by                            AS verified_by,
                 c1.verified_datetime                      AS verified_datetime,
                 c1.date_verified_by_supervisor            AS supervised_datetime,
                 substr(d1.amended_text, 0, 25000)         AS report_text,
                 h1.dmis_id_code                           AS location_code,
                 i1.facility_name                          AS location_name
             FROM
                radiology_reports_74 a1
                INNER JOIN radiology_exam_70_5           b1 
                    ON a1.site_id = b1.site_id
                    AND a1.patient_name = b1.name
                LEFT JOIN sub_amended_report_74_05      c1 
                    ON a1.site_id = c1.site_id
                    AND a1.ien = c1.radiology_reports_74
                LEFT  JOIN sub_amended_text_74_06        d1 
                    ON a1.site_id = c1.site_id
                    AND d1.sub_amended_report_74_05 = c1.ien
                LEFT JOIN provider_6                    e1 
                    ON c1.site_id = e1.site_id
                    AND c1.interpreting_radiologist = e1.ien
                LEFT JOIN provider_6                    e2
                    ON c1.site_id = e2.site_id
                    AND c1.approving_radiologist = e2.ien
                LEFT JOIN result_category_78_3          f1 
                    ON c1.site_id = f1.site_id
                    AND c1.status = f1.ien
                LEFT JOIN user_3                        g1 
                    ON c1.site_id = g1.site_id
                    AND c1.verified_by = g1.ien
                LEFT JOIN medical_center_division_40_8        h1 
                    ON c1.site_id = h1.site_id
                    AND c1.amending_recorders_division = h1.ien
                LEFT JOIN dmis_id_codes_8103                  i1 
                    ON h1.site_id = i1.site_id
                    AND h1.dmis_id_code = i1.dmis_id
            WHERE a1.site_id = 63
                  AND  b1.ien = examid;

        dbms_sql.return_result(refcursor);
    END get_rad_amendments;

    PROCEDURE getradclinicalimpression (
        orderid VARCHAR2 DEFAULT '0'
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN
        OPEN refcursor FOR SELECT
                               b1.clinical_impression_extended AS clinical_impression_extended
                             FROM
                                    order_101 a1
                                 JOIN sub_clinical_impression_extended_101_01  b1
                               ON a1.site_id = b1.site_id
                                  AND b1.order_101 = a1.ien
                                 JOIN patient_2                                c1
                               ON a1.site_id = c1.site_id
                                  AND a1.patient = c1.ien
                            WHERE
                                   a1.ien = orderid
                                  AND a1.site_id = 63;

        dbms_sql.return_result(refcursor);
    END getradclinicalimpression;

    PROCEDURE get_pdts_allergy_profile (
        patientid VARCHAR2 DEFAULT '0'
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN
        OPEN refcursor FOR SELECT
                               b1.allergy_selection_bngnn    AS allergy,
                               b1."COMMENT"                  AS "COMMENT",
                               b1.reaction_comment           AS reaction_comment,
                               'UNK'                         AS reactions
                           FROM
                                    clinical_history_8810 a1
                               JOIN sub_drug_allergy_8810_03      b1 ON a1.site_id = b1.site_id
                                                                   AND a1.ien = b1.clinical_history_8810
                               LEFT JOIN allergies_selections_8254_01  c1 ON a1.site_id = c1.site_id
                                                                            AND b1.allergy_selection = c1.ien
                               JOIN patient_2                     d1 ON a1.ien = d1.ien
                                                    AND a1.site_id = d1.site_id
                                                    AND a1.ien = d1.ien
                           WHERE
                                   d1.ien = patientid
                               AND a1.site_id = 63;

        dbms_sql.return_result(refcursor);
    END get_pdts_allergy_profile;

    PROCEDURE get_patient_immunizations (
        patientid IN VARCHAR2 DEFAULT '0'
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN
        OPEN refcursor FOR SELECT
                               a1.datetime_administered    AS administerdt,
                               b1.name                     AS vaccine,
                               a1.dose_number              AS dosenumber,
                               c1.manufacturer             AS manufacturer,
                               c1.lot                      AS lotnumber,
                               a1.dose                     AS dose,
                               a1.dose_units               AS doseunits,
                               a1.next_dose_due_date       AS nextdosedt,
                               a1.results                  AS result,
                               a1.result_comment           AS resultcomment
                           FROM
                                    immunization_events_52008 a1
                               JOIN vaccine_type_52016_2  b1 ON a1.site_id = b1.site_id
                                                               AND a1.vaccine_type = b1.ien
                               JOIN lot_file_52009        c1 ON a1.site_id = c1.site_id
                                                         AND a1.lot = c1.ien
                               JOIN patient_2             d1 ON a1.site_id = d1.site_id
                                                    AND a1.patientien = d1.ien
                           WHERE
                                   d1.ien = patientid
                               AND a1.site_id = 63;

        dbms_sql.return_result(refcursor);
    END get_patient_immunizations;

    PROCEDURE get_patient_encounters (
        patientid  IN  VARCHAR2,
        startdate  IN  DATE,
        stopdate   IN  DATE,
        "Max"      IN  Number,
        pAll      IN   Number
    ) AS
        refcursor     SYS_REFCURSOR;
        v_start_date  DATE;
        v_end_date    DATE;
    BEGIN
        v_start_date := nvl(startdate, sysdate - 540);
        v_end_date := nvl(stopdate, sysdate - 1);
        --patientid = '41586'
        OPEN refcursor FOR 
            SELECT * FROM (
                SELECT
                   a1.appointment_datetime AS appt_datetime,
                   k1.name AS clinic_name,
                   f1.appointment_status AS appointment_status,
                   g1.description AS appt_type,
                   d1.name AS provider_name,
                   a1.reason_for_appointment AS reason_for_appointment,
                   a1.clinic AS clinic_id,
                   a1.provider AS provider_id,
                   d1.name AS provider_rank,
                   e1.name AS provider_class,
                   c1.provider_id AS provider_ident,
                   c1.DUTY_PHONE_1 AS provider_work_phone,
                   c1.pager_ AS provider_pager,
                   a1.ien AS appt_id,
                   b1.ien AS encounter_id,
                   'Test Data' as diagnosis
                   --j1.icd_diagnosis AS diagnosis
                FROM
                  patient_appointment_44_2 a1
                   inner join encounter_311 b1
                   on a1.encounter_ptr = b1.ien
                   and a1.site_id = b1.site_id
                   inner join provider_6 c1
                   on a1.provider = c1.ien
                   and a1.site_id = c1.site_id
                   inner join MILITARY_GRADE_RANK_8104 d1
                   on c1.rank = d1.ien
                   and c1.site_id = d1.site_id
                   inner join provider_class_7 e1
                   on c1.class = e1.ien
                   and c1.site_id = e1.site_id
                   inner join APPOINTMENT_STATUS_8514 f1
                   on a1.appointment_status = f1.ien
                   and a1.site_id = f1.site_id
                   inner join APPOINTMENT_type_44_5 g1
                   on a1.appointment_type = g1.ien
                   and a1.site_id = g1.site_id
                   inner join hospital_location_44 k1
                   on a1.clinic = k1.ien
                   and a1.site_id = k1.site_id
                   left join KG_ADC_DATA_100501 h1
                   on a1.encounter_ptr = h1.ien
                   and a1.site_id = h1.site_id
                   /*inner join sub_diagnosis_100501_01 j1
                   on h1.ien = j1.KG_ADC_DATA_100501
                   and h1.site_id = j1.site_id
                   */
                   where b1.patient = patientId
                   and a1.appointment_datetime between v_start_date and v_end_date
                   and a1.cancel_flag is null)
                where (pAll = '0' and rownum >= "Max")
                    or pAll = '1';

        dbms_sql.return_result(refcursor);
    END get_patient_encounters;

    PROCEDURE get_pat_prescriptions (
        patientid     IN  VARCHAR2,
        earliestdate  IN  DATE DEFAULT sysdate - 548,
        latestdate    IN  DATE DEFAULT sysdate + 1,
        activeonly    IN  NUMBER DEFAULT 1
    ) AS
        refcursor     SYS_REFCURSOR;
        v_start_date  DATE;
        v_end_date    DATE;
    BEGIN
    --LOCAL VARIABLE
        --  v_start_date
        OPEN refcursor FOR SELECT
                               prescription_52.login_date              AS login_date,
                               prescription_52.last_fill_date          AS last_fill_date,
                               drug_50.name                            AS drug,
                               prescription_52.refills                 AS refills,
                               prescription_52.sig                     AS sig,
                               provider_6.name                         AS provider_name,
                               prescription_52.days_supply             AS days_supply,
                               prescription_52.comments                AS comments,
                               prescription_52.qty                     AS qty,
                               prescription_52.expiration_date         AS expiration_date,
                               outpatient_site_59_2.name               AS pharmacy,
                               prescription_52.rx_                     AS rx_number,
                               prescription_52.ien                     AS rx_id,
                               drug_50.content_unit                    AS content_unit,
                               prescription_52.refills_remaining       AS refills_remaining,
                               prescription_52.drug                    AS drug_id,
                               dmis_id_codes_8103.dmis_id              AS ops_dmis_id,
                               prescription_52.outpatient_site_name    AS ops_facility_name,
                               prescription_52.status                  AS status,
                               dmis_id_codes_8103.dmis_id              AS dmis_id,
                               dmis_id_codes_8103.facility_name        AS facility_name,
                               prescription_52.order_entry_number      AS order_entry_number,
                               prescription_52.child_resistant_cont    AS child_resistant_cont,
                               prescription_52.expanded_sig            AS expanded_sig,
                               order_101.expanded_sig                  AS order_sig
                           FROM
                               prescription_52,
                               dmis_id_codes_8103,
                               order_101,
                               outpatient_site_59_2,
                               provider_6,
                               medical_center_division_40_8,
                               drug_50
                           WHERE
                                   prescription_52.drug = drug_50.rowid
                               AND prescription_52.site_id = drug_50.site_id
                               AND prescription_52.provider = provider_6.rowid
                               AND prescription_52.site_id = provider_6.site_id
                               AND prescription_52.last_dispensing_pharmacy = outpatient_site_59_2.rowid
                               AND prescription_52.site_id = outpatient_site_59_2.site_id
                               AND prescription_52.outpatient_site = outpatient_site_59_2.rowid
                               AND outpatient_site_59_2.mtf_division = medical_center_division_40_8.rowid
                               AND outpatient_site_59_2.site_id = medical_center_division_40_8.site_id
                               AND medical_center_division_40_8.dmis_id_code = dmis_id_codes_8103.rowid
                               AND medical_center_division_40_8.site_id = dmis_id_codes_8103.site_id
                               AND prescription_52.mtf_division = medical_center_division_40_8.rowid
                               AND prescription_52.site_id = medical_center_division_40_8.site_id
                               AND prescription_52.order_pointer = order_101.rowid
                               AND prescription_52.site_id = order_101.site_id
                               AND prescription_52.site_id = 63
                           ORDER BY
                               1,
                               2 DESC;

        dbms_sql.return_result(refcursor);
    END get_pat_prescriptions;

    PROCEDURE getprescriptionfills (
        rxid IN VARCHAR2
    ) AS
        refcursor SYS_REFCURSOR;
	    --LOCAL VARIABLE
    BEGIN
    	--LOCAL VARIABLE
        OPEN refcursor FOR SELECT
                               sub_fill_dates_52_01.fill_dates     AS fill_dates,
                               sub_fill_dates_52_01.fill_number    AS fill_number,
                               sub_fill_dates_52_01.logged_by      AS entered_by,
                               sub_fill_dates_52_01.qty            AS qty,
	    --SUB_AGE_RELATED_DOSAGES_50_02.UNIT_OF_MEASURE AS UNIT_TYPE,
                               drug_50.default_unit                AS units_for_qty,
                               outpatient_site_59_2.name           AS pharmacy,
                               sub_fill_dates_52_01.action         AS action_code,
                               sub_fill_dates_52_01.fill_cost      AS fill_cost,
                               drug_50.content_unit                AS content_unit,
                               dmis_id_codes_8103.dmis_id          AS ops_dmis_id,
                               dmis_id_codes_8103.facility_name    AS ops_facility_name,
                               dmis_id_codes_8103.dmis_id          AS dmis_id,
                               dmis_id_codes_8103.facility_name    AS facility_name
                           FROM
                               prescription_52,
                               sub_fill_dates_52_01,
                               dmis_id_codes_8103,
                               outpatient_site_59_2,
                               medical_center_division_40_8,
                               drug_50
                           WHERE
                                   sub_fill_dates_52_01.outpatient_site = outpatient_site_59_2.rowid
                               AND sub_fill_dates_52_01.site_id = outpatient_site_59_2.site_id
                               AND prescription_52.drug = drug_50.rowid
                               AND prescription_52.site_id = drug_50.site_id
                               AND sub_fill_dates_52_01.mtf_division = medical_center_division_40_8.rowid
                               AND sub_fill_dates_52_01.site_id = medical_center_division_40_8.site_id
                               AND medical_center_division_40_8.dmis_id_code = dmis_id_codes_8103.rowid
                               AND medical_center_division_40_8.site_id = dmis_id_codes_8103.site_id
                               AND prescription_52.mtf_division = medical_center_division_40_8.rowid
                               AND prescription_52.site_id = medical_center_division_40_8.site_id
                               AND prescription_52.site_id = 63
                           ORDER BY
                               1,
                               2 DESC;

        dbms_sql.return_result(refcursor);
    END getprescriptionfills;

    PROCEDURE get_patient_notes_cda (
        patientid       VARCHAR2 DEFAULT '0',
        earliestdate    DATE,
        latestdate      DATE,
        notetype        VARCHAR2 DEFAULT 'DCPO'
    ) AS
        refcursor SYS_REFCURSOR;
        v_start_date  DATE;
        v_end_date    DATE;
    BEGIN
        v_start_date := nvl(earliestdate, sysdate - 540);
        v_end_date := nvl(latestdate, sysdate - 1);
        OPEN refcursor FOR
---- ###### PROGRESS NOTES ##########
         SELECT
                               'PROGRESS NOTE'         AS notetype,
                               a1.type_of_note_type    AS notetypename,
                               a1.entered_by           AS verifiedby,
                               a1.note_date_time       AS verifieddate,
                               c1.type                 AS notetitle,
                               to_char(b1.note)        AS note,
                               NULL                    AS admissiondate,
                               NULL                    AS dischargedate,
                               NULL                    AS dictatedby,
                               NULL                    AS status,
                               a1.ien                  AS noteid
                           FROM
                               gp_progress_note_file_52013  a1,
                               sub_note_52013_01            b1,
                               progress_note_title_52002    c1,
                               patient_2                    d1
                           WHERE
                                   a1.site_id = b1.site_id
                               AND a1.site_id = c1.site_id
                               AND a1.site_id = d1.site_id
                               AND b1.gp_progress_note_file_52013 = a1.ien
                               AND a1.type_of_note = c1.ien
                               AND a1.patient = d1.ien
                               AND d1.ien = patientid
                               AND a1.note_date_time BETWEEN v_start_date AND v_end_date  --earliest and latest dates
                               AND a1.site_id = 63
                           UNION ALL
  ---- ###### DISCHARGE NOTES ##########
                           SELECT
                               '18842-5'                       AS notetype,
                               'DISCHARGE SUMMARIZATION NOTE'  AS notetypename,
                               c1.name                         AS verifiedby,
                               a1.physician_signature_date     AS verifieddate,
                               NULL                            AS notetitle,
                               'Need S_D_N_52000_7'            AS note,
                               b1.admission_date               AS admissiondate,
                               b1.disposition_date             AS dischargedate,
                               c2.name                         AS dictatedby,
                               a1.working_status               AS status,
                               a1.ien                          AS noteid
                           FROM
                               gp_discharge_summary_52000  a1,
                               sub_admission_date_2_95     b1,
                               user_3                      c1,
                               user_3                      c2,
                               patient_2                   d1
                           WHERE
                                   a1.site_id = b1.site_id
                               AND a1.site_id = c1.site_id
                               AND a1.site_id = c2.site_id
                               AND a1.site_id = d1.site_id
                               AND a1.admission_pointer = b1.ien
                               AND b1.patient_2 = d1.ien
                               AND a1.physician_signature = c1.ien
                               AND a1.nar_sum_physician = c2.ien
                               AND a1.patient = d1.ien
                               AND d1.ien = patientid
                               AND a1.creation_date BETWEEN v_start_date AND v_end_date --earliest and latest dates
                               AND a1.site_id = 63
                           UNION ALL
  --- ######## CONSULT NOTES ##############
                           SELECT
                               '11488-4'                 AS notetype,
                               'CONSULTATION NOTE'       notetypename,
                               a1.verify_by_name         AS verify_by,
                               a1.datetime_completed     AS verifieddate,
                               c1.name                   AS notetitle,
                               'Need SUB_NOTE_52003_02'  AS note,
                               NULL                      AS admissiondate,
                               NULL                      AS dischargedate,
                               NULL                      AS dictatedby,
                               NULL                      AS status,
                               a1.ien                    AS noteid
                           FROM
                               gp_consult_result_52003    a1,
                               patient_appointment_44_2   b1,
                               ancillary_procedure_108_1  c1,
                               order_101                  d1,
                               patient_2                  e1
                           WHERE
                                   a1.site_id = b1.site_id
                               AND a1.site_id = c1.site_id
                               AND a1.site_id = d1.site_id
                               AND a1.site_id = e1.site_id
                               AND a1.appointment = b1.ien
                               AND d1.patient = e1.ien
                               AND d1.ancillary_procedure = c1.ien
                               AND a1.patient_name = e1.ien
                               AND e1.ien = patientid
                               AND a1.datetime_completed BETWEEN v_start_date AND v_end_date --earliest and latest dates
                               AND a1.site_id = 63
                           UNION ALL
  --- ############ ENCOUNTER NOTES ################
                           SELECT
                               '34108-1'                       AS notetype,
                               'OUTPATIENT ENCOUNTER'          notetypename,
                               a1.last_editted_by_name         AS verify_by,
                               a1.last_edit_datetime           AS verifieddate,
                               c1.name                         AS notetitle,
                               'Not Sure how to get the note'  AS note,
                               NULL                            AS admissiondate,
                               NULL                            AS dischargedate,
                               NULL                            AS dictatedby,
                               NULL                            AS status,
                               a1.ien                          AS noteid
                           FROM
                               kg_adc_data_100501        a1,
                               patient_appointment_44_2  b1,
                               hospital_location_44      c1,
                               patient_2                 d1
                           WHERE
                                   a1.site_id = b1.site_id
                               AND a1.site_id = c1.site_id
                               AND a1.site_id = d1.site_id
                               AND a1.appointment = b1.ien
                               AND b1.clinic = c1.ien
                               AND b1.name = d1.ien
                               AND d1.ien = patientid
                               AND a1.last_edit_datetime BETWEEN v_start_date AND v_end_date
                               AND a1.site_id = 63;

        dbms_sql.return_result(refcursor);
    END get_patient_notes_cda;

    PROCEDURE get_patient_problem_list_bydate (
        patientid     IN  VARCHAR2,
        earliestdate    IN DATE,
        latestdate    IN DATE
    ) AS
        refcursor     SYS_REFCURSOR;
        v_start_date  DATE;
        v_end_date    DATE;
    BEGIN
        v_start_date := nvl(earliestdate, sysdate - 548);
        v_end_date := nvl(latestdate, sysdate);

            OPEN refcursor FOR SELECT
                b1.ien AS problem_id,
                d1.code_number AS diagnosis,
                d1.diagnosis AS problem,
                d1.DESCRIPTION AS longdescription,
                'I' as inout,
                b1.datetime as encounter_date,
                NULL AS onsetdate,
                NULL AS "COMMENT",
                NULL AS status,
                NULL AS acuity,
                NULL as entering_user,
                NULL AS entered_date,
                b1.HOSPITAL_LOCATION_NAME as location,
                b1.provider_NAME as provider,
                NULL as last_MODIFIED_DATE
            FROM
               encounter_311 b1
                inner join sub_diagnosis_311_12 c1
                    ON b1.site_id = c1.site_id
                    AND b1.ien = c1.encounter_311
                INNER JOIN icd_diagnosis_80 d1
                    ON c1.site_id = d1.site_id
                    AND c1.diagnosis = d1.ien
                LEFT JOIN sub_comment_100417_02 e1
                    ON c1.site_id = e1.site_id
                    AND c1.ien = e1.kg_patient_diagnosis_100417
            where 
                b1.patient = patientid      --'428380'
                and b1.datetime between v_start_date and v_end_date
            union all
                SELECT
                    c1.ien as problem_id,
                    d1.code_number AS diagnosis,
                    NVL(d1.diagnosis, regexp_substr(c1.problem, ';([[:print:]]{1,})', 1, 1, 'i', 1)) AS problem,
                    d1.DESCRIPTION AS longdescription,
                    'O' as inout,
                    c1.date_of_onset as encounter_date,
                    c1.date_of_onset AS onsetdate,
                    e1."COMMENT" AS "COMMENT",
                    c1.status AS status,
                    c1.acuity AS acuity,
                    c1.ENTERING_USER_NAME as entering_user,
                    c1.date_entered AS entered_date,
                    c1.LOCATION_NAME as location,
                    c1.provider_NAME as provider,
                    c1.MODIFIED_DATE as last_MODIFIED_DATE
                FROM
                   kg_patient_diagnosis_100417 c1
                    LEFT JOIN icd_diagnosis_80 d1
                        ON c1.site_id = d1.site_id
                        AND c1.diagnosis = d1.code_number
                    LEFT JOIN sub_comment_100417_02 e1
                        ON c1.site_id = e1.site_id
                        AND c1.ien = e1.kg_patient_diagnosis_100417
                where  
                    c1.patient = patientid       --:patId
                    and  c1.date_of_onset between v_start_date and v_end_date 
                order by encounter_date;

        dbms_sql.return_result(refcursor);
    END get_patient_problem_list_bydate;

    PROCEDURE get_patient_procedures (
        patientid     IN  VARCHAR2,
        earliestdate  IN  DATE,
        latestdate    IN  DATE
    ) AS
        refcursor     SYS_REFCURSOR;
        v_start_date  DATE;
        v_end_date    DATE;
    BEGIN
        v_start_date := nvl(earliestdate, sysdate - 540);
        v_end_date := nvl(latestdate, sysdate - 1);

            OPEN refcursor FOR
                SELECT
                    b1.patient,
                    c1."NUMBER" AS procedure_id,
                    d1.code_number AS code,
                    d1.DESCRIPTION AS PROCEDURE,
                    b1.datetime AS encounter_date,
                    e1.clinical_record_status AS status,
                    b1.hospital_location_name AS LOCATION,
                    'I' AS "TYPE"
                FROM
                    encounter_311 b1
                    INNER JOIN  sub_icd_operationsprocedures_311_14 c1
                        ON b1.site_id = c1.site_id
                        AND b1.ien = c1.encounter_311
                    INNER JOIN icd_operationprocedure_80_1 d1
                        ON c1.site_id = d1.site_id
                        AND c1.icd_operationsprocedures = d1.ien
                    INNER JOIN sub_admission_date_2_95 e1
                        ON b1.site_id = e1.site_id
                        AND b1.ien = regexp_substr(e1.encounter, '[[:digit:]]{1,}')
                WHERE 
                    b1.patient = patientid
                    AND b1.datetime BETWEEN v_start_date AND v_end_date
                    AND b1.site_id = 63
                UNION ALL
                SELECT a1.patient_2,
                    c1.ien AS procedure_id,
                    d1.cpt_code AS code,
                    e1.long_description AS PROCEDURE,
                    b1.appointment_datetime AS encounter_date,
                    c1.caper_status AS status,
                    c1.geographic_location_name AS LOCATION,
                    'O' AS "TYPE"
                FROM
                    sub_admission_date_2_95 a1
                    INNER JOIN   patient_appointment_44_2 b1
                    ON a1.patient_2 = b1.NAME
                    AND a1.site_id = b1.site_id
                    INNER JOIN  kg_adc_data_100501 c1
                    ON b1.ien = c1.appointment
                    AND b1.site_id = c1.site_id
                    INNER JOIN sub_cpt_code_100501_02 d1
                        ON c1.site_id = d1.site_id
                        AND c1.ien = d1.kg_adc_data_100501
                    INNER JOIN sub_cpt_code_100501_02 f1
                    ON c1.site_id = f1.site_id
                        AND c1.ien = f1.kg_adc_data_100501
                    INNER JOIN cpthcpcs_8151 e1
                        ON e1.site_id = f1.site_id
                        AND e1.code = f1.cpt_code
                WHERE 
                    a1.patient_2 = patientid
                    AND b1.appointment_datetime BETWEEN v_start_date AND v_end_date
                    AND b1.site_id = 63
                ORDER BY 4 DESC;

        dbms_sql.return_result(refcursor);
    END get_patient_procedures;

    PROCEDURE get_snomedcodes (
        patientid  IN  VARCHAR2,
        resultid   IN  VARCHAR2
    ) AS
        refcursor SYS_REFCURSOR;
    --LOCAL VARIABLE
    BEGIN
    --LOCAL VARIABLE
        OPEN refcursor FOR SELECT
                               topography_field_61.snomed_code    AS code,
                               topography_field_61.snomed_code    AS container,
                               morphology_field_61_1.name         AS description
                           FROM
                               morphology_field_61_1,
                               topography_field_61
                           WHERE
                                   topography_field_61.snomed_code = morphology_field_61_1.ien
                               AND topography_field_61.site_id = morphology_field_61_1.site_id
                               AND topography_field_61.site_id = 63
                           ORDER BY
                               1,
                               2 DESC;

        dbms_sql.return_result(refcursor);
    END get_snomedcodes;

    PROCEDURE get_laborder_list (
        patientid           VARCHAR2 DEFAULT '0',
        earliestdate        DATE DEFAULT NULL,
        latestdate          DATE DEFAULT NULL,
        targetsubcriptlist  VARCHAR2 DEFAULT 'NA'
    ) AS
        refcursor     SYS_REFCURSOR;
        v_start_date  DATE;
        v_end_date    DATE;
    BEGIN
        v_start_date := nvl(earliestdate, sysdate - 548);
        v_end_date := nvl(latestdate, sysdate);
        OPEN refcursor FOR SELECT
                               nvl(m1.order_id_number, b1.id_number)          AS order_number,
                               nvl(m1.ordered_test, b1.lab_test)              AS test_id,
                               b1.order_comment                               AS order_comment,
                               b1.order_datetime                              AS order_datetime,
                               a1.ien                                         AS order_id,
                               d1."NAME"                                      AS ordering_provider,
                               b1.activity                                    AS order_status,
                               c1."NAME"                                      AS order_priority,
                               f1."NAME"                                      AS requesting_location,
                               nvl(m1.ordered_test_name, h1."NAME")           AS test_name,
                               b1.datetime_of_collection                      AS collection_datetime,
                               b1.collection_sample_name                      AS collection_sample,
                               nvl(j1."NAME", 'UNKNOWN')                      AS specimen_name,
                               m1.specimen_name                               AS specimen,
                               a1."TRACKING"                                  AS accession,
                               g1.dmis_id                                     AS location_code,
                               g1.facility_name                               location_name,
                               a1.task_status                                 AS task_status,
                               a1.ien                                         AS result_id
                           FROM
                                    order_task_107 a1
                               JOIN order_101                     b1 ON a1.site_id = b1.site_id
                                                    AND a1."ORDER" = b1.ien
                               JOIN order_priority_102_3          c1 ON b1.site_id = c1.site_id
                                                               AND b1."PRIORITY" = c1.ien
                               JOIN provider_6                    d1 ON b1.site_id = d1.site_id
                                                     AND b1.ordering_hcp = d1.ien
                               JOIN hospital_location_44          f1 ON a1.site_id = f1.site_id
                                                               AND a1.requesting_location = f1.ien
                               JOIN hospital_location_44          f2 ON b1.site_id = f2.site_id
                                                               AND b1.patient_location = f2.ien
                               JOIN medical_center_division_40_8  e1 ON f2.site_id = e1.site_id
                                                                       AND f2.division = e1.ien
                               JOIN dmis_id_codes_8103            g1 ON e1.site_id = g1.site_id
                                                             AND e1.dmis_id_code = g1.ien
                               JOIN lab_test_60                   h1 ON b1.site_id = h1.site_id
                                                      AND b1.lab_test = h1.ien
                               JOIN collection_sample_62          i1 ON b1.site_id = i1.site_id
                                                               AND b1.collection_sample = i1.ien
                               JOIN topography_field_61           j1 ON b1.site_id = j1.site_id
                                                              AND b1.specimen_site = j1.ien
                               JOIN accession_66                  k1 ON a1.site_id = k1.site_id
                                                       AND a1."TRACKING" = k1.current_accession_name
                               JOIN patient_2                     l1 ON a1.site_id = l1.site_id
                                                    AND a1.patient = l1.ien
                                                    AND k1.patient = l1.ien
                               LEFT OUTER JOIN pli_central_info_8738         m1 ON a1.site_id = m1.site_id
                                                                           AND a1.ien = m1.order_task
                           WHERE
                                   a1.patient = patientid
                               AND b1.order_datetime BETWEEN v_start_date AND v_end_date
                               AND h1.subscript IN (
                                   'CLINICAL CHEMISTRY',
                                   'BACTERIOLOGY',
                                   'PARASITOLOGY',
                                   'MYCOLOGY',
                                   'VIROLOGY',
                                   'CYTOLOGY GYN',
                                   'BONE MARROW',
                                   'BLOOD BANK',
                                   'AUTOPSY',
                                   'SURGICAL PATHOLOGY',
                                   'CYTOLOGY NON-GYN'
                               )
                               AND a1.site_id = 63;

        dbms_sql.return_result(refcursor);
    END get_laborder_list;


    PROCEDURE get_specimens (
        resultid IN VARCHAR2
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN

            OPEN refcursor FOR 
                SELECT
                    c1.container                                                       AS container,
                    c1.frozen_section                                                  AS frozen,
                    CASE
                        WHEN regexp_substr(a1.result_reference, '^[[:alpha:]]{1,2}', 1) = 'CG' THEN
                            d1.name
                        ELSE
                             d1.name
                            || CHR(10) || chr(9) 
                            || f1.specimen_comment 
                    END AS description,
                    d1.name AS topography
                FROM
                         order_task_107                         a1
                    INNER JOIN order_101                        b1 
                        ON a1.order_id_number = b1.id_number
                        AND a1.site_id = b1.site_id
                    LEFT JOIN sub_specimen_pathology_101_08     c1 
                        ON b1.ien = c1.order_101
                        AND b1.site_id = c1.site_id
                    LEFT JOIN sub_topography_specimen_101_19    e1 
                        ON c1.row_id = e1.sub_specimen_pathology_101_08
                        AND c1.site_id = e1.site_id
                    LEFT JOIN topography_field_61               d1 
                        ON e1.topography_specimen = d1.ien
                        AND e1.site_id = d1.site_id
                    LEFT JOIN (
                        SELECT
                            sub_specimen_pathology_101_08,
                            LISTAGG(enter_specimen, 'chr(10)') WITHIN GROUP(
                                ORDER BY
                                    enter_specimen
                            ) AS specimen_comment,
                            site_id
                        FROM
                            sub_enter_specimen_101_09
                        GROUP BY
                            sub_specimen_pathology_101_08,
                            site_id
                        ORDER BY
                            sub_specimen_pathology_101_08
                    ) f1 ON c1.row_id = f1.sub_specimen_pathology_101_08
                            AND c1.site_id = f1.site_id
                WHERE
                        a1.site_id = 63
                    AND a1.result_reference IS NOT NULL;

        dbms_sql.return_result(refcursor);
    END get_specimens;

    PROCEDURE get_labmicrobiology_result (
        taskid    IN  VARCHAR2,
        resultid  IN  VARCHAR2,
        user      IN  VARCHAR2
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN
        IF taskid = '1890' THEN
            OPEN refcursor FOR SELECT
                                   'TARGET CELLS'                          AS test_name,
                                   '960911 OP 9'                           AS accession,
                                   'INTERNAL MEDICINE'                     AS req_location,
                                   'HAIL, DAVID H'                         AS ordering_provider,
                                   '970218-00340'                          AS order_id,
                                   'BLOOD'                                 AS collection_sample,
                                   'NTF'                                   AS result,
                                   'NO BACTERIA SEEN'                      AS gram_status,
                                   '1'                                     AS status,
                                   'Amended Final Report'                  AS status_description,
                                   '1865'                                  AS test_id,
                                   'CONTROL IS FOR POSITIVE GROWTH'        AS sterility_control,
                                   'NO GROWTH'                             AS sterility_result,
                                   'NO GROWTH IN 48 HOURS'                 AS "COMMENT",
                                   TO_DATE('2004-03-20', 'yyy-mm-dd')      AS certified_date,
                                   'BAILEYXXX, GREEN'                      AS certify_by,
                                   'CLINICAL CHEMISTRY'                    AS micro_type,
                                   'T043'                                  AS performing_location_code,
                                   'USAHC'                                 AS performing_location_name,
                                   NULL                                    AS is_sensitive
                               FROM
                                   dual;

        ELSE
            OPEN refcursor FOR SELECT
                                   NULL  AS test_name,
                                   NULL  AS accession,
                                   NULL  AS req_location,
                                   NULL  AS ordering_provider,
                                   NULL  AS order_id,
                                   NULL  AS collection_sample,
                                   NULL  AS result,
                                   NULL  AS gram_status,
                                   NULL  AS status,
                                   NULL  AS status_description,
                                   NULL  AS test_id,
                                   NULL  AS sterility_control,
                                   NULL  AS sterility_result,
                                   NULL  AS "COMMENT",
                                   NULL  AS certified_date,
                                   NULL  AS certify_by,
                                   NULL  AS micro_type,
                                   NULL  AS performing_location_code,
                                   NULL  AS performing_location_name,
                                   NULL  AS is_sensitive
                               FROM
                                   dual;

        END IF;

        dbms_sql.return_result(refcursor);
    END get_labmicrobiology_result;

    PROCEDURE get_labmicro_orgsensitivity (
        patientid  IN  VARCHAR2,
        resultid   IN  VARCHAR2,
        testid     IN  VARCHAR2,
        orgid      IN  VARCHAR2
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN
        IF testid = '1868' THEN
            OPEN refcursor FOR SELECT
                                   '1868'                         AS test_ien,
                                   '407'                          AS organism_number,
                                   '407'                          AS organism_id,
                                   'BETA STREPTOCOCCCUS GROUP A'  AS organism_name,
                                   'STEPTOMYCIN'                  AS antibiotic_name,
                                   '1000'                         AS print_order,
                                   'S'                            AS sensitivity,
                                   'SUSCEPTIBLE'                  AS iinterpretation
                               FROM
                                   dual;

        ELSE
            OPEN refcursor FOR SELECT
                                   NULL  AS test_ien,
                                   NULL  AS organism_number,
                                   NULL  AS organism_id,
                                   NULL  AS organism_name,
                                   NULL  AS antibiotic_name,
                                   NULL  AS print_order,
                                   NULL  AS sensitivity,
                                   NULL  AS iinterpretation
                               FROM
                                   dual;

        END IF;

        dbms_sql.return_result(refcursor);
    END get_labmicro_orgsensitivity;

    PROCEDURE get_labmicro_organisms (
        patientid  IN  VARCHAR2,
        resultid   IN  VARCHAR2,
        testid     IN  VARCHAR2
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN
        IF testid = '2050' THEN
            OPEN refcursor FOR SELECT
                                   '2050'              AS test_ien,
                                   '1'                 AS organism_number,
                                   '1'                 AS organism_id,
                                   'ESCHERICHIA COLI'  AS organism_name,
                                   '>100,000 CFU/ML'   AS quantity,
                                   '1'                 AS stage,
                                   '0'                 AS has_sensitivities
                               FROM
                                   dual;

        ELSE
            OPEN refcursor FOR SELECT
                                   NULL  AS test_ien,
                                   NULL  AS organism_number,
                                   NULL  AS organism_id,
                                   NULL  AS organism_name,
                                   NULL  AS quantity,
                                   NULL  AS stage,
                                   NULL  AS has_sensitivities
                               FROM
                                   dual;

        END IF;

        dbms_sql.return_result(refcursor);
    END get_labmicro_organisms;

    PROCEDURE get_lab_chemistry_result (
        patientid  IN  VARCHAR2,
        resultid   IN  VARCHAR2,
        testid     IN  VARCHAR2
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN
        IF testid = '1856' THEN
            OPEN refcursor FOR SELECT
                                   'WKG'                                                                          AS certifying_person,
                                   'ALERT'                                                                        AS alert,
                                   'Chemistrty, which states that the valuesgreater than the upper limit of the'  AS interpretation,
                                   'CHEMIATRY DIRECT COOMBS'                                                      AS test_name,
                                   'TEST RESULT PURGED BY DE-INDENTIFY'                                           AS result,
                                   'RESULT VERIFIED BY REPEAT ANALYSIS'                                           AS "COMMENT",
                                   '.2 - .5'                                                                      AS normal_range,
                                   'ML'                                                                           AS units,
                                   TO_DATE('2004-03-20', 'yyyy-mm-dd')                                            AS certify_datetime,
                                   'P'                                                                            AS result_code,
                                   'PENDING'                                                                      AS result_code_desc,
                                   '1856'                                                                         AS test_id,
                                   '1986'                                                                         AS location_code,
                                   'FT BENNING'                                                                   AS location_name
                               FROM
                                   dual;

        ELSE
            OPEN refcursor FOR SELECT
                                   NULL  AS certifying_person,
                                   NULL  AS alert,
                                   NULL  AS interpretation,
                                   NULL  AS test_name,
                                   NULL  AS result,
                                   NULL  AS "COMMENT",
                                   NULL  AS normal_range,
                                   NULL  AS units,
                                   NULL  AS certify_datetime,
                                   NULL  AS result_code,
                                   NULL  AS result_code_desc,
                                   NULL  AS test_id,
                                   NULL  AS location_code,
                                   NULL  AS location_name
                               FROM
                                   dual;

        END IF;

        dbms_sql.return_result(refcursor);
    END get_lab_chemistry_result;

    PROCEDURE get_lab_pathology_result (
        patientid  IN  VARCHAR2,
        resultid   IN  VARCHAR2
    ) AS
        refcursor SYS_REFCURSOR;
    BEGIN
        IF patientid = '' THEN
            OPEN refcursor FOR SELECT
                                   'MATEOXXX,RED J'                         AS test_name,
                                   'TEST COMMENT'                           AS result_comment,
                                   'TEST COMMENT'                           AS full_report,
                                   ''                                       AS performing_location_name,
                                   ''                                       AS performing_location_code,
                                   TO_DATE('2010-11-05', 'yyyy-mm-dd')      AS certified_datetime,
                                   'COMPLETED'                              AS result_status,
                                   'COLLINSXXX, PINK'                       AS certified_by
                               FROM
                                   dual;

        ELSE
            OPEN refcursor FOR SELECT
                                   NULL  AS test_name,
                                   NULL  AS result_comment,
                                   NULL  AS full_report,
                                   NULL  AS performing_location_name,
                                   NULL  AS performing_location_code,
                                   NULL  AS certified_datetime,
                                   NULL  AS result_status,
                                   NULL  AS certified_by
                               FROM
                                   dual;

        END IF;

        dbms_sql.return_result(refcursor);
    END get_lab_pathology_result;

END jointventureinterface;
/